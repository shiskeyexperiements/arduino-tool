﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ArduinoDriver;
using ArduinoDriver.SerialProtocol;
using ArduinoUploader;

namespace arduino_tool_light
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ArduinoDriver.ArduinoDriver arduinoDriver;
        private ImageSource Connected = new BitmapImage(new Uri(@"Static/Green.png", UriKind.Relative));
        private ImageSource Disconnected = new BitmapImage(new Uri(@"Static/Red.png", UriKind.Relative));
        private String port = "COM4";
        public MainWindow()
        {
            InitializeComponent();
            InitializeArduino();
        }


        private void InitializeArduino()
        {
            try
            {
                CurrentPort.Text = port;
                arduinoDriver = new ArduinoDriver.ArduinoDriver(ArduinoUploader.Hardware.ArduinoModel.UnoR3, port, true);
                ArduinoStatus.Content = "Arduino Successfully connected on port " + port;
                Thread.Sleep(100);
                UpdatePortStatus();
            }
            catch (IOException e)
            {
                ArduinoStatus.Content = "Arduino Not connected. Please check port";
                Console.WriteLine(e.ToString());
            }
            catch(UnauthorizedAccessException e)
            {
                ArduinoStatus.Content = "Arduino Busy. Remove and connect again.";
                Console.WriteLine(e);
            }
        }

        #region Update Button Status
        private void UpdatePortStatus()
        {

            foreach (Button button in FindVisualChildren<Button>(this))
            {
                try
                {
                    byte.Parse((String)button.Content);
                }
                catch (Exception)
                {

                    continue;
                }
                byte buttonNo = byte.Parse((String)button.Content);
                Console.WriteLine(buttonNo);
                DigitalReadRequest digitalReadRequest = new DigitalReadRequest(buttonNo);
                DigitalReadResponse digitalReadResponse = arduinoDriver.Send(digitalReadRequest);
                Thread.Sleep(10);
                switch (buttonNo)
                {
                    case 13:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status13.Source = Connected;
                        } 
                        else
                        {
                            Status13.Source = Disconnected;
                        }
                        break;
                    case 12:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status12.Source = Connected;
                        }
                        else
                        {
                            Status12.Source = Disconnected;
                        }
                        break;
                    case 11:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status11.Source = Connected;
                        }
                        else
                        {
                            Status11.Source = Disconnected;
                        }
                        break;
                    case 10:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status10.Source = Connected;
                        }
                        else
                        {
                            Status10.Source = Disconnected;
                        }
                        break;
                    case 9:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status9.Source = Connected;
                        }
                        else
                        {
                            Status9.Source = Disconnected;
                        }
                        break;
                    case 8:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status8.Source = Connected;
                        }
                        else
                        {
                            Status8.Source = Disconnected;
                        }
                        break;
                    case 7:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status7.Source = Connected;
                        }
                        else
                        {
                            Status7.Source = Disconnected;
                        }
                        break;
                    case 6:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status6.Source = Connected;
                        }
                        else
                        {
                            Status6.Source = Disconnected;
                        }
                        break;
                    case 5:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status5.Source = Connected;
                        }
                        else
                        {
                            Status5.Source = Disconnected;
                        }
                        break;
                    case 4:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status4.Source = Connected;
                        }
                        else
                        {
                            Status4.Source = Disconnected;
                        }
                        break;
                    case 3:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status3.Source = Connected;
                        }
                        else
                        {
                            Status3.Source = Disconnected;
                        }
                        break;
                    case 2:
                        if (digitalReadResponse.PinValue == DigitalValue.High)
                        {
                            Status2.Source = Connected;
                        }
                        else
                        {
                            Status2.Source = Disconnected;
                        }
                        break;

                    default:
                        break;
                }
                // do something with tb here
            }


        }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            togglePin(13);
        }

        private void togglePin(byte v)
        {
            // Check current status and toggle
            DigitalReadRequest digitalReadRequest = new DigitalReadRequest(v);
            DigitalReadResponse digitalReadResponse = arduinoDriver.Send(digitalReadRequest);
            Thread.Sleep(50);

            if (digitalReadResponse.PinValue == DigitalValue.High)
            {
                DigitalWriteRequest digitalWriteRequest = new DigitalWriteRequest(v, DigitalValue.Low);
                DigitalWriteReponse digitalWriteReponse = arduinoDriver.Send(digitalWriteRequest);
                Thread.Sleep(50);
                Console.WriteLine("Wrote " + digitalWriteReponse.PinValue.ToString() + " to pin " + digitalWriteReponse.PinWritten);
            }
            else
            {
                DigitalWriteRequest digitalWriteRequest = new DigitalWriteRequest(v, DigitalValue.High);
                DigitalWriteReponse digitalWriteReponse =arduinoDriver.Send(digitalWriteRequest);
                Thread.Sleep(50);
                Console.WriteLine("Wrote " + digitalWriteReponse.PinValue.ToString() + " to pin " + digitalWriteReponse.PinWritten);
            }
            UpdatePortStatus();

        }

        #region To Enumerate All Objects of a control
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        #endregion

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            togglePin(12);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            togglePin(11);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            togglePin(10);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            UpdatePortStatus();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            togglePin(9);
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            togglePin(8);
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            togglePin(7);
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            togglePin(6);
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            togglePin(5);
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            togglePin(4);
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            togglePin(3);
        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            togglePin(2);
        }

        private void Button_Click_13(object sender, RoutedEventArgs e)
        {

            foreach (Button button in FindVisualChildren<Button>(this))
            {
                try
                {
                    byte.Parse((String)button.Content);
                }
                catch (Exception)
                {

                    continue;
                }
                byte buttonNo = byte.Parse((String)button.Content);
                Console.WriteLine(buttonNo);
                Console.WriteLine(arduinoDriver.Send(new PinModeRequest(buttonNo, PinMode.Output)));
            }
        }

        private void Button_Click_14(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(@"Static\CDM21228_Setup.exe");

        }

        private void Button_Click_15(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("This is written in the port box ==> " + CurrentPort.Text);
            port = CurrentPort.Text.Trim().ToUpper();
            InitializeArduino();
        }
    }
}
